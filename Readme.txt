El alcance de la aplicacion es el CRUD completo mediante el navegador y el API de Create y Read

-> Navegador:
	home: http://localhost:8000/aplicacion/
	Create: http://localhost:8000/aplicacion/crear/
	Read: http://localhost:8000/aplicacion/listar/
	Update: http://localhost:8000/aplicacion/editar/(documento usuario)/
	Delete: http://localhost:8000/aplicacion/eliminar/(documento usuario)/

-> Api:
	Create: http://localhost:8000/api/v1/personas/
	Read: http://localhost:8000/api/v1/personas/

Para utilizar el API de CREATE es necesario hacer la peticion POST enviando los par�metros mediante el body,
en caso de obviar alguno de los par�mtetros requeridos el API generar� un mensaje indicando cual(es) falta(n).

La parte del Login, el Update y Delete del API no est�n incluidos en la aplicaci�n.

Muchas gracias por su tiempo y atenci�n. Feliz d�a.