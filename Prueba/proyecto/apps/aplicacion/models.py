from django.db import models

class Persona(models.Model):
    nombre = models.CharField(max_length = 200)
    apellidos = models.CharField(max_length = 250)
    edad = models.IntegerField()
    telefono = models.CharField(max_length = 12)


class Personas(models.Model):
    documento = models.CharField(primary_key = True, max_length = 12)
    nombre = models.CharField(max_length = 200)
    apellidos = models.CharField(max_length = 250)
    fechaNacimiento = models.DateField()

    def __str__(self):
        return self.nombre + ' ' + self.apellidos