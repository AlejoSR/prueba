from django import forms
from .models import Personas

class PersonasForm(forms.ModelForm):
    class Meta:
        model = Personas
        fields = [
            'documento',
            'nombre',
            'apellidos',
            'fechaNacimiento',
        ]

class LoginForm(forms.Form):
    username = forms.CharField(max_length=30, required = True, label = "", 
        widget=(forms.TextInput(attrs={"placeholder":"@Nombre de usuario","class":"input-login"})))
    userpassword = forms.CharField(max_length=30, required = True, label = "", 
        widget=(forms.PasswordInput(attrs={"placeholder":"Contraseña","class":"input-login"})))