from django.shortcuts import render,redirect,get_object_or_404
from .models import Personas
from .forms import *
from rest_framework import generics
from .serializers import PersonaSerializer
from django.contrib.auth import authenticate, login

def home(request):
    return render(request,'aplicacion/index.html')

def index(request):
    form = LoginForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            data = form.cleaned_data
            usuario = data.get("username")
            contraseña = data.get("userpassword")
            print(usuario)
            print(contraseña)
    var = {
        "form_login":form,
    }

def CrearPersona(request):
    if request.method == 'POST':
        form = PersonasForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('listar')
    else:
        form = PersonasForm()
    return render(request,'aplicacion/crear_persona.html',{'form':form})

def ListarPersonas(request):
    personas = Personas.objects.all()
    context = {'personas': personas}
    print(context)
    return render(request,'aplicacion/listar_personas.html',context)

def EditarPersona(request, documento):
    persona = Personas.objects.get(documento = documento)
    if request.method == 'GET':
        form = PersonasForm(instance=persona)
    else:
        form = PersonasForm(request.POST, instance=persona)
        if form.is_valid():
            form.save()
        return redirect('listar')
    return render(request, 'aplicacion/crear_persona.html',{'form':form})

def EliminarPersona(request, documento):
    persona = Personas.objects.get(documento = documento)
    if request.method == 'POST':
        persona.delete()
        return redirect('listar')
    return render(request, 'aplicacion/eliminar_persona.html',{'persona':persona})

# SERIALIZADORES

class Listar(generics.ListCreateAPIView):
    queryset = Personas.objects.all()
    serializer_class = PersonaSerializer

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(
            queryset,
            pk=self.kwargs['pk'],
        )
        return obj