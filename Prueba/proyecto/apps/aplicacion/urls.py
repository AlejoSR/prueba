from django.conf.urls import url
from .views import *
from django.urls import path

urlpatterns = [
    path('',home, name="index"),
    path('login',index, name="login"),
    path('crear/', CrearPersona, name="crear"),
    path('listar/', ListarPersonas, name="listar"),
    url(r'^editar/(?P<documento>\d+)/$', EditarPersona, name="editar"),
    url(r'^eliminar/(?P<documento>\d+)/$', EliminarPersona, name="eliminar"),
    url(r'^personas/$', Listar.as_view(), name='personas')
]