from .models import Personas
from rest_framework import serializers

class PersonaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Personas
        fields = ('documento','nombre','apellidos','fechaNacimiento')