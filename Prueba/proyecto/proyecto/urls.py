from django.contrib import admin
from django.urls import path,include
from django.conf import settings
from django.conf.urls import url, include
from rest_framework.authtoken import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('aplicacion/',include('apps.aplicacion.urls')),
    path('api/v1/', include(('apps.aplicacion.urls', 'personas'), namespace='personas'))
]

urlpatterns += [
    path('api/v1/auth', include(('rest_framework.urls','rest_framework'), namespace='rest_framework'))
]
